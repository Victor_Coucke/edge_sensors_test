/*
SharpDistSensorBasic.ino
Source: https://github.com/DrGFreeman/SharpDistSensor
*/
#include "Seeed_vl53l0x.h"
Seeed_vl53l0x VL53L0X;

#ifdef ARDUINO_SAMD_VARIANT_COMPLIANCE
#define SERIAL SerialUSB
#else
#define SERIAL Serial
#endif
#include "SharpDistSensor.h"
#define ANALOG_REFERENCE 3.3
#define ADC_RESOLUTION 4095
// Analog pin to which the sensor is connected
const byte sensorPin = 36;
int i = 0;
// Window size of the median filter (odd number, 1 = no filtering)
const byte medianFilterWindowSize = 5;

// Create an object instance of the SharpDistSensor class
SharpDistSensor sensor(sensorPin, medianFilterWindowSize, ANALOG_REFERENCE, ADC_RESOLUTION);

void setup()
{
  Serial.begin(9600);
  // Set sensor model
  sensor.setModel(SharpDistSensor::GP2Y0A51SK0F_5V_DS);
  pinMode(34, INPUT); // digital
  pinMode(27, INPUT); //sharp digital

  VL53L0X_Error Status = VL53L0X_ERROR_NONE;
  Status = VL53L0X.VL53L0X_common_init();
  if (VL53L0X_ERROR_NONE != Status)
  {
    SERIAL.println("start vl53l0x mesurement failed!");
    VL53L0X.print_pal_error(Status);
    while (1)
      ;
  }
  VL53L0X.VL53L0X_continuous_ranging_init();
  if (VL53L0X_ERROR_NONE != Status)
  {
    SERIAL.println("start vl53l0x mesurement failed!");
    VL53L0X.print_pal_error(Status);
    while (1)
      ;
  }
  //Serial.println("CLEARDATA"); // clears sheet starting at row 2
  Serial.println("CLEARSHEET"); // clears sheet starting at row 1

  // define 5 columns named "Date", "Time", "Timer", "Counter" and "millis"
  Serial.println("LABEL,Date,Time,Timer,sharp_analog,sharp_digital,ToF");

  // set the names for the 3 checkboxes
  Serial.println("CUSTOMBOX1,LABEL,Stop logging at 250?");
  Serial.println("CUSTOMBOX2,LABEL,Resume log at 350?");
  Serial.println("CUSTOMBOX3,LABEL,Quit at 450?");

  // check 2 of the 3 checkboxes (first two to true, third to false)
  Serial.println("CUSTOMBOX1,SET,1");
  Serial.println("CUSTOMBOX2,SET,1");
  Serial.println("CUSTOMBOX3,SET,0");
}

void loop()
{
  // Get distance from sensor
  unsigned int distance = sensor.getDist();

  // // Print distance to Serial
  // Serial.print("edge =");
  // Serial.println(distance);
  //   Serial.print("IR_digital =");
  // Serial.println(digitalRead(12));

  VL53L0X_RangingMeasurementData_t RangingMeasurementData;
  VL53L0X.PerformContinuousRangingMeasurement(&RangingMeasurementData);
  // if (RangingMeasurementData.RangeMilliMeter >= 2000) {
  //     SERIAL.println("TOF : out of ranger");
  // } else {
  //     SERIAL.print("TOF : distance::");
  //     SERIAL.println(RangingMeasurementData.RangeMilliMeter);
  // }
  // Wait some time
  // alternative writing method:
  Serial.print("DATA,DATE,TIME,TIMER,");
  if (distance > 190)
  {
    Serial.print(190);
  }
  else Serial.print(distance);
  Serial.print(",");
  Serial.print(!digitalRead(34));
  Serial.print(",");
  if (RangingMeasurementData.RangeMilliMeter > 190) Serial.print(190);
  else Serial.print(RangingMeasurementData.RangeMilliMeter);
  Serial.print(",");
  Serial.println("SCROLLDATA_20");

  delay(500);
}